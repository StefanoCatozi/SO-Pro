#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"
#include "funzioni.h"

#define PORT 22500
#define PORTUDP 22501



int window;
int sockTCP;
int sockUDP;
int my_id;
int flag;
int num_vehicles_att;
struct sockaddr_in* sockaddrUDP;

WorldViewer viewer;
World world;
Vehicle* vehicle; // The vehicle

  

void sig_handler(int signo,siginfo_t* siginfo, void* context){
  printf("HANDLER\n");
  PacketHeader head;
  head.type = GetId;
  head.size = sizeof(IdPacket);
  IdPacket* id = (IdPacket*)malloc(sizeof(IdPacket));
  id->header = head;
  id->id = my_id;
   if(id == NULL) printf("\t\t\tsing_PACK INFAME");
  InviaUDP(sockUDP,sockaddrUDP,(PacketHeader*)id);
  printf("\n\n\nESCO INVIATO UDP\n");
  flag = 0;
  close(sockTCP);
  //close(sockUDP);
  exit(0);
  free(id);
  }

void AggiungiPlayer(int id,struct sockaddr_in* sockaddr){
    printf("AGGIUNGO PLAYER\n");
  PacketHeader* rcv;
  PacketHeader head;
  ImagePacket* send = (ImagePacket*)malloc(sizeof(ImagePacket));
  ImagePacket* img;
  Image* texture;
  
  head.type = GetTexture;
  head.size = sizeof(ImagePacket);
  send->header = head;
  send->id = id;
  send->image = NULL;
  
  IdPacket* id_pack = (IdPacket*)malloc(sizeof(IdPacket));
  head.type = GetId;
  head.size = sizeof(IdPacket);
  id_pack->header = head;
  id_pack->id = my_id;
  InviaUDP(sockUDP,sockaddr,(PacketHeader*)send);
  InviaUDP(sockUDP,sockaddr,(PacketHeader*)id_pack);
  rcv = RiceviPacchettoTCP(sockTCP);
  img = (ImagePacket*)rcv;
  texture = img->image;
  printf("Ricevuta Texture per player id:%d\n",id);
  Vehicle* new = (Vehicle*)malloc(sizeof(Vehicle));
  Vehicle_init(new,&world,id,texture);
  new = World_addVehicle(&world,new);
  free(send);
  free(id_pack);
  return;
}

void EliminaPlayer(int id){
  Vehicle* v = World_getVehicle(&world,id);
  World_detachVehicle(&world,v);
  World_update(&world);
  printf("ELIMINATO PLAYER ID:%d\n",id);
  return;
}

void AggiornaMondo(WorldUpdatePacket* update,struct sockaddr_in* sockaddr){
    printf("AGGIORNA MONDO\n");
  int num = update->num_vehicles;
  int i=0;
  if(num_vehicles_att == 1 && num != 1 ){
      while(i<num){
        ClientUpdate client = update->updates[i];
        if(client.id == my_id){
            i++;
            continue;
        }
        AggiungiPlayer(client.id,sockaddr);
        printf("\tAggiunto id:%d\n",client.id);
        i++;
    }
    num_vehicles_att =  num;
  }
  
  if(num<num_vehicles_att){
    i = 0;
    int flag_while = 1;
    ListHead veicoli_mondo = world.vehicles;
    ClientUpdate* veicoli_update = update->updates;
    ClientUpdate up;
    ListItem* item = veicoli_mondo.first;
    while(i<num){
      up = veicoli_update[i];
      Vehicle* v = (Vehicle*)item;
      if(up.id != v->id){
         EliminaPlayer(v->id);
         flag_while = 0;
         break;
       }
      item = item->next;
      i++;
    }
    if(flag_while){
      while(i<num_vehicles_att){
        Vehicle* v = (Vehicle*)item;
        EliminaPlayer(v->id);
        item = item->next;
        i++;
      }
    }
    num_vehicles_att = num;
  }
  
  if(num>num_vehicles_att){
    ClientUpdate* veicoli_update = update->updates;
    ClientUpdate up;
    //int diff = num_vehicles_att-num;
    i=num_vehicles_att;
    
    while(i<num){
      up = veicoli_update[i];
      if(up.id == my_id) {
          i++;
          continue;
      }
      printf("\tAggiungo Nuovo veicolo con id:%d\n",up.id);
      AggiungiPlayer(up.id,sockaddr);
      i++;
    }
    num_vehicles_att = num;
  }
  i=0;
  ClientUpdate client_update;
  Vehicle* vec;
  while(i<num){
    client_update = update->updates[i];
    vec = World_getVehicle(&world,client_update.id);
    vec->x = client_update.x;
    vec->y = client_update.y;
    vec->theta = client_update.theta;
    i++;
  }
  World_update(&world);
}

void* HandlerConnessione(void* args){
  flag = 1;
  PacketHeader* update;
  
  VehicleUpdatePacket* vupd_pack = (VehicleUpdatePacket*)malloc(sizeof(VehicleUpdatePacket));
  PacketHeader header;
  header.type = VehicleUpdate;
  vupd_pack->header = header;
  vupd_pack->id = my_id;
  while(flag){        //METTERE UN SIGNALHANDLER
    vupd_pack->rotational_force = vehicle->rotational_force_update;
    vupd_pack->translational_force = vehicle->translational_force_update;
    InviaUDP(sockUDP,sockaddrUDP,(PacketHeader*)vupd_pack);
    printf("ASPETTO_UPDATE_WORLD\n");
    update = RiceviPacchettoUDP(sockUDP,sockaddrUDP);
    
    //NEW PALYER????
    if(update == NULL) printf("UPDATE_NULL\n");
    AggiornaMondo((WorldUpdatePacket*)update,sockaddrUDP);
    /*case VehicleUpdate: AggiungiPlayer((VehicleUpdatePacket*)update,arg->sockaddr); //TEST AGGIUNGERE PLAYER
                        break;*/                                                      //CON SEGNALAZIONE DA SERVER
    
    
    usleep(90000);
  }
  free(vupd_pack);
}

int main(int argc, char **argv) {
  if (argc<3) {
    printf("usage: %s <server_address> <player texture>\n", argv[1]);
    exit(-1);
  }

  printf("loading texture image from %s ... ", argv[2]);
  Image* my_texture = Image_load(argv[2]);
  if (my_texture) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }
  
  Image* my_texture_for_server;
  // todo: connect to the server
  //   -get ad id
  //   -send your texture to the server (so that all can see you)!!!!!!!!!!!
  //   -get an elevation map
  //   -get the texture of the surface
  
  struct sigaction act ;
  memset(&act,0,sizeof(struct sigaction));
  act.sa_flags = SA_SIGINFO;
  act.sa_sigaction = sig_handler;
  int res = sigaction(SIGINT,&act,NULL);
  if(res){
    printf("SDADASD\n");
    return 12;
  }
  
  //CONNESSIONE
  char* ip = argv[1];
  struct sockaddr_in* sckaddr = malloc(sizeof(struct sockaddr_in));
  sockTCP = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if(sockTCP < 0){
    fprintf(stderr,"CLIENT: Errore creazione socket\n");
    return 0;
  }
  
  sckaddr->sin_family = AF_INET;
  sckaddr->sin_addr.s_addr = inet_addr(ip);
  sckaddr->sin_port = htons(PORT);
  int n = sizeof(*sckaddr);
  if(connect(sockTCP,(struct sockaddr*) sckaddr, n)<0){
    perror("CLIENT:Errore connessione");
  }
  printf("Connessione TCP avviata\n");
  //INIZIALIZZAZIONE
  // these come from the server
  Image* map_elevation;
  Image* map_texture;
  Image* my_texture_from_server;
  
  //Prendo id
  num_vehicles_att = 1;
  PacketHeader head;
  PacketHeader* pack;
  
  IdPacket* id_pack = (IdPacket*)malloc(sizeof(IdPacket));
  head.type = GetId;
  head.size = sizeof(IdPacket);
  id_pack->header = head;
  id_pack->id = -1;
  InviaTCP((PacketHeader*)id_pack,sockTCP);
  printf("ID Inviato\n");
  pack = RiceviPacchettoTCP(sockTCP);
  printf("RICEVUTO PACCHETTO\n");
  id_pack = (IdPacket*)pack;
  my_id = id_pack->id;
  printf("Ottenuto id:%d\n",my_id);
  //MANDO TEXTURE !!!!
  ImagePacket* img = (ImagePacket*)malloc(sizeof(ImagePacket));
  head.type = PostTexture;
  head.size = sizeof(ImagePacket);
  img->header = head;
  img->id = my_id;
  img->image = my_texture;
  InviaTCP((PacketHeader*)img,sockTCP);
  printf("Texture Inviata\n");
  //Prendo Elevation
  head.type = GetElevation;
  img->header = head;
  img->id = my_id;
  InviaTCP((PacketHeader*)img,sockTCP);
  pack = RiceviPacchettoTCP(sockTCP);
  img = (ImagePacket*)pack;
  map_elevation = img->image;
  printf("Ricevuta Elevation Map\n");
  //Prendo Surface
  head.type = GetTexture;
  img->header =  head;
  img->id = my_id;
  InviaTCP((PacketHeader*)img,sockTCP);
  printf("Inviata Surface\n");
  pack = RiceviPacchettoTCP(sockTCP);
  img = (ImagePacket*)pack;
  map_texture = img->image;
  printf("Ricevuta Surface\n");
  //MYTEXTURE
  pack = RiceviPacchettoTCP(sockTCP);
  img = (ImagePacket*)pack;
  my_texture_from_server = img->image;
  printf("Ricevuta Texture dal server \n");
  //my_texture_from_server = my_texture //?????????
  // construct the world
  World_init(&world, map_elevation, map_texture, 0.5, 0.5, 0.5);
  vehicle=(Vehicle*) malloc(sizeof(Vehicle));
  Vehicle_init(vehicle, &world, my_id, my_texture_from_server);
  World_addVehicle(&world, vehicle);

  // spawn a thread that will listen the update messages from
  // the server, and sends back the controls
  // the update for yourself are written in the desired_*_force
  // fields of the vehicle variable
  // when the server notifies a new player has joined the game
  // request the texture and add the player to the pool
  /*FILLME*/
  
  sockUDP = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  
  struct sockaddr_in* udp = malloc(sizeof(struct sockaddr_in));
  udp->sin_family = AF_INET;
  udp->sin_addr.s_addr = inet_addr(ip);
  udp->sin_port = htons(PORTUDP);
  
  if(connect(sockUDP,(struct sockaddr*) udp,sizeof(struct sockaddr_in))<0){
    fprintf(stderr,"CLIENT:Errore connessione\n");
  }
  printf("Aperta Connessione UDP\n");
  sockaddrUDP = udp;
  pthread_t thread;
  pthread_create(&thread,NULL,HandlerConnessione,NULL);
  
  printf("Thread UDP partito\n");
  WorldViewer_runGlobal(&world, vehicle, &argc, argv);
    pthread_join(thread,NULL);
  // cleanup
  free(udp);
  free(sckaddr);
  free(img);
  free(id_pack);
  World_destroy(&world);
  return 0;             
}



 
 
 

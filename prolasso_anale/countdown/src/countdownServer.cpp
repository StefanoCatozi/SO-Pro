#include <ros/ros.h>
#include <ros/time.h>
#include <actionlib/server/simple_action_server.h>
#include <countdown/CountdownAction.h>
using namespace std;

class CountdownAction
{
protected:

  ros::NodeHandle nh;
  actionlib::SimpleActionServer<countdown::CountdownAction> countserver; 
  std::string action_name_;
  countdown::CountdownResult fine;

public:

  CountdownAction(std::string name) :
    countserver(nh, name, boost::bind(&CountdownAction::count, this, _1), false),
    action_name_(name)
  {
    countserver.start();
    ROS_INFO("Aspetto la connessione di un client");
  }

void count(const countdown::CountdownGoalConstPtr &goal)
  {
    ROS_INFO("Eseguo il countdown");

    //eseguo l azione
    for(int i = goal->number; i > 0; i--){
      cout << i << endl;
			if(countserver.isPreemptRequested() || !ros::ok()){
				ROS_INFO("STOPPATO");
				countserver.setPreempted();
				break;
			}
		sleep(2);
    }
    ROS_INFO("Finito");
    //terminazione dello stato(come nuovo goal)
    fine.executed = true;
    countserver.setSucceeded(fine);
  }
};
int main(int argc, char** argv)
{
  ros::init(argc, argv, "server");
  
  //classe
  CountdownAction countdown("countdownServer");
  ros::spin();

  return 0;
}

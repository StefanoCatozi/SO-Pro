
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"

#define BUF 1000000
#define BUFUDP 1500

void InviaUDP(int sock,struct sockaddr_in* sockaddr,PacketHeader* pack){
  char buf[BUFUDP],size[4];
  int sended=0,tot,ret;
  tot = Packet_serialize(buf,pack);
  
  while(sended<tot){
    ret = sendto(sock,buf,BUFUDP,0,(struct sockaddr*)sockaddr,sizeof(struct sockaddr_in));
    if(ret<0){
      perror("CLIENT:Errore invio UDP");
      return;
    }
    sended+=ret;
  }
  int* s = (int*)size;
    *s = tot;
    ret = sendto(sock,size,4,0,(struct sockaddr*)sockaddr,sizeof(struct sockaddr_in));
    if(ret<0){
            perror("CLIENT:Errore invio UDP SIZE");
            return;
        }
    return;
}

PacketHeader* RiceviPacchettoUDP(int sock,struct sockaddr_in* sockaddr){
  char buf[BUFUDP],size[4];
  int ret;
  int dim = sizeof(struct sockaddr_in);
  ret = recvfrom(sock,buf,BUFUDP,0,(struct sockaddr*)sockaddr,(socklen_t*)&dim);
  if(ret<0){
    fprintf(stderr,"CLIENT:Errore ricezione UDP");
    return NULL;
  }
  ret = recvfrom(sock,size,4,0,(struct sockaddr*)sockaddr,(socklen_t*)&dim);
  if(ret<0){
    perror("CLIENT:Errore ricezione UDP SIZE");
    return NULL;
  }
  int* s = (int*)size;
  if(buf == NULL) printf("\t\tBUF_NULL");
  PacketHeader* re = Packet_deserialize(buf,*s);
  if(re == NULL) printf("RE==NULL\n");
  return re;
}

void InviaTCP(PacketHeader* packet, int sock){
	char buf[BUF],size[4];
    int tot,sended=0,ret;
    tot = Packet_serialize(buf,packet);
    while(sended<tot){
        ret = send(sock,buf+sended,BUF,0);
        if(ret<0){
            perror("CLIENT:Errore invio TCP");
            return;
        }
        sended += ret;
    }
    int* s = (int*)size;
    *s = tot;
    ret = send(sock,size,4,0);
    if(ret<0){
            perror("CLIENT:Errore invio TCP SIZE");
            return;
        }
    return;
}

PacketHeader* RiceviPacchettoTCP(int sock){
	char buf[BUF],size[4];
	int ret;
  ret = recv(sock,buf,BUF,MSG_WAITALL);
  if(ret<0){
    perror("CLIENT:Errore ricezione TCP");
    return NULL;
  }
  ret = recv(sock,size,4,MSG_WAITALL);
  if(ret<0){
    perror("CLIENT:Errore ricezione TCP SIZE");
    return NULL;
  }
  int* s = (int*)size;
  return Packet_deserialize(buf,*s);
}  


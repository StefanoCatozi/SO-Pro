#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"
#include "common.h"
#include "funzioni.h"

#define max_client 3
#define max_connessioni 3
#define TCP_port 22500
#define UDP_port 22501

World mondo;
Image* surface_texture;
Image* surface_elevation;
Image* vehicle_texture;
int client_att;

WorldUpdatePacket* newWUP(World* w){
	PacketHeader h;
	h.type = WorldUpdate;
  h.size = sizeof(WorldUpdate);
	ClientUpdate* c =(ClientUpdate*) malloc(sizeof(ClientUpdate)*(w->vehicles.size));

	ListItem* item=w->vehicles.first;
	int i=0;
  for(int i=0;item;i++){
    Vehicle* v=(Vehicle*)item;
    c[i].id=v->id;
    c[i].x =v->x;
    c[i].y =v->y;
    c[i].theta =v->theta;
    item=item->next;

 	}

 	WorldUpdatePacket* p = (WorldUpdatePacket*)malloc(sizeof(WorldUpdatePacket));
 	p->header = h;
 	p->num_vehicles = w->vehicles.size;
 	p->updates = c;
  return p;
}
void EliminaAtutti(int id){
	 printf("funzione\n");
	 ListHead veicoli_mondo = mondo.vehicles;
	 ListItem* item = veicoli_mondo.first;
	 Vehicle* vec;
	 PacketHeader head;
	 head.type = GetId;
	 head.size = sizeof(IdPacket);
	 IdPacket* canc;
	 canc->header = head;
	 while(item){
		 printf("ciclo\n");
		 vec = (Vehicle*)item;
		 canc->id = id;
		 InviaTCP((PacketHeader*)canc,vec->sockTCP);
		 item = item->next;
	 }
 }
		 
	 
void* gestisci_udp(void* args){
	int ret;
	int sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	ERROR_HELPER(sock, "Errore socket udp");

	struct sockaddr_in addr, other_addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_port = htons(UDP_port);

	ret=bind(sock, (struct sockaddr*) &addr, sizeof(addr));
	ERROR_HELPER(ret, "Errore bind udp");

	Vehicle* vehicle_aux;
	//char buf[1024];
	int len=sizeof(other_addr);

	PacketHeader* packet;
	VehicleUpdatePacket* v_update;
	ImagePacket* texture;
	ImagePacket* img_packet;
	IdPacket* esc;
	Vehicle* v;

	while(1){
		struct sockaddr_in other_addr;
		packet=RiceviPacchettoUDP(sock, &other_addr);
    if(packet == NULL){
      printf("PACKET_NULL\n");
      usleep(500000);
      continue;
    }
		switch(packet->type){
			case GetTexture:   
						img_packet=(ImagePacket*) packet;
						vehicle_aux=World_getVehicle(&mondo, img_packet->id);
						PacketHeader head;
						ImagePacket* send=(ImagePacket*)malloc(sizeof(ImagePacket));
						head.type=PostTexture;
						head.size=sizeof(ImagePacket);
						send->header=head;
						send->id=img_packet->id;
						send->image=vehicle_aux->texture;
						IdPacket* id = (IdPacket*)RiceviPacchettoUDP(sock,&other_addr);
						Vehicle* vec = World_getVehicle(&mondo,id->id);
						InviaTCP((PacketHeader*)send,vec->sockTCP);
						free(send);
						break;

			case VehicleUpdate: 
						v_update =(VehicleUpdatePacket*) packet;
						vehicle_aux=World_getVehicle(&mondo, v_update->id);
						vehicle_aux->translational_force_update=v_update->translational_force;
						vehicle_aux->rotational_force_update=v_update->rotational_force;
						World_update(&mondo);             
						InviaUDP(sock, &other_addr,(PacketHeader*)newWUP(&mondo));
						break;
						
			case GetId:
						esc=(IdPacket*) packet;
						printf("Il client:id=%d si è disconnesso\n", esc->id);
						v=World_getVehicle(&mondo, esc->id);
						World_detachVehicle(&mondo, v);
						client_att--;
						printf("diovavasd\n");
						EliminaAtutti(esc->id);
						break;
			default: break;
		}
	}
	//pthread_exit(NULL);
}

Image* collegamento(int sock_client, IdPacket* pacchetto_id, Image* v_texture){
	int ret;
  InviaTCP((PacketHeader*) pacchetto_id, sock_client);
  
  Image* clientTexture;
  PacketHeader* pack;
  ImagePacket* clientImage;
  PacketHeader head;
  
	pack = RiceviPacchettoTCP(sock_client); 
  clientImage = (ImagePacket*) pack;
	
	clientTexture=clientImage->image;
	
	ImagePacket* mapElevation=(ImagePacket*) RiceviPacchettoTCP(sock_client);
  ImagePacket* send_map = (ImagePacket*)malloc(sizeof(ImagePacket));
  head.type = PostElevation;
  head.size = sizeof(ImagePacket);
	send_map->header = head;
  send_map->id=0;
  send_map->image=surface_elevation;
  InviaTCP((PacketHeader*) send_map, sock_client);
  ImagePacket* mapTexture=(ImagePacket*) RiceviPacchettoTCP(sock_client);
  
  mapTexture->header = head;
  mapTexture->id=0;
  mapTexture->image=surface_texture;
  
  InviaTCP((PacketHeader*) mapTexture, sock_client);
		
  ImagePacket* rinv=(ImagePacket*)malloc(sizeof(ImagePacket));
	PacketHeader h;
	h.type=PostTexture;
	h.size=sizeof(ImagePacket);
	rinv->header=h;
	rinv->id=pacchetto_id->id;
	rinv->image=vehicle_texture;
	InviaTCP((PacketHeader*)rinv,sock_client);
		
  if (clientImage->header.type==GetTexture){
   clientImage->header.type=PostTexture;
   clientImage->id=0;
   clientImage->image=v_texture;
   clientTexture=v_texture;
   InviaTCP((PacketHeader*) clientImage, sock_client);
  }
	
  free(rinv);
  free(send_map);
  return clientTexture;
}


int main(int argc, char **argv) {
  if (argc<3) {
    printf("usage: %s <elevation_image> <texture_image>\n", argv[1]);
    exit(-1);
  }
  char* elevation_filename=argv[1];
  char* texture_filename=argv[2];
  char* vehicle_texture_filename="./images/diocane.ppm";
  printf("loading elevation image from %s ... ", elevation_filename);

  // load the images
  surface_elevation = Image_load(elevation_filename);
  if (surface_elevation) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }


  printf("loading texture image from %s ... ", texture_filename);
  surface_texture = Image_load(texture_filename);
  if (surface_texture) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }

  printf("loading vehicle texture (default) from %s ... ", vehicle_texture_filename);
  vehicle_texture = Image_load(vehicle_texture_filename);
  if (vehicle_texture) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }
  World_init(&mondo,surface_elevation,surface_texture,0.5,0.5,0.5);
  pthread_t udp,disconnessi;
  int ret;
  ret=pthread_create(&udp,NULL,gestisci_udp,NULL);
  PTHREAD_ERROR_HELPER(ret,"Errore thread udp");
  /*ret=pthread_create(&disconnessi,NULL,connessioni,NULL);
	PTHREAD_ERROR_HELPER(ret,"Errore thread disconnessi");*/
	
	int id=1;
	int sock=socket(AF_INET,SOCK_STREAM,0);
	ERROR_HELPER(sock,"Errore creazione socket");
	
	struct sockaddr_in server_addr;
  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  server_addr.sin_port = htons(TCP_port);
	
	int size_sock=sizeof(struct sockaddr_in);
  ret=bind(sock, (struct sockaddr*) &server_addr, size_sock);
  ERROR_HELPER(ret, "Errore bind");

  ret=listen(sock, max_connessioni);
  ERROR_HELPER(ret, "Errore listen");
  
  int client_sock;
  struct sockaddr_in *client_addr = calloc(1, sizeof(struct sockaddr_in));
  	
	Vehicle* vehicle;
	client_att = 0;
	while(1){
		client_sock=accept(sock, (struct sockaddr*)client_addr, (socklen_t*) &size_sock);
	  ERROR_HELPER(client_sock, "Errore accept");

	  IdPacket* pacchetto_id=(IdPacket*) RiceviPacchettoTCP(client_sock);
    if (pacchetto_id==NULL) continue;

    pacchetto_id->id=id;
    Image* clientTexture;
	clientTexture=collegamento(client_sock, pacchetto_id, vehicle_texture); 
   

		vehicle=(Vehicle*) malloc(sizeof(Vehicle));
		Vehicle_init(vehicle, &mondo, pacchetto_id->id, clientTexture);
		vehicle->sockTCP = client_sock;
		printf("Giocatore con id:%d è presente nel mondo\n",pacchetto_id->id);
		World_addVehicle(&mondo, vehicle);
		client_att++;
		
		client_addr = calloc(1, sizeof(struct sockaddr_in));
		id++;
	}
	World_destroy(&mondo);
  return 0;             
}

#include "linked_list_mine.h"
#include "string.h"

linked_list* linked_list_new(){
	linked_list* ris=(linked_list*) malloc(sizeof(linked_list));
	ris->size=0;
	ris->head=NULL;
	return ris;
}

linked_list_node* linked_list_get(linked_list* l, int id){
	//if (!linked_list_find(l, id)) return NULL;

	linked_list_node* aux=l->head;
	while(aux!=NULL){
		if (aux->id==id) return aux;
		aux=aux->next;
	}
	return NULL;
}

linked_list_node* linked_list_add(linked_list* l, int id, int tcp_socket){
	if (l==NULL) return NULL;
	linked_list_node* ris=(linked_list_node*) malloc(sizeof(linked_list_node));
	ris->id=id;
	ris->tcp_socket=tcp_socket;
	ris->next=l->head;
	l->head=ris;
	l->size+=1;
	return ris;
}

void linked_list_delete(linked_list* l, int id){
	//if (!linked_list_find(l, id)) return;

	linked_list_node* aux=l->head;
	l->size-=1;

	if (aux->id==id){
		l->head=aux->next;
		free(aux);
		return;
	}

	while(aux->next!=NULL){
		if (aux->next->id==id){
			aux->next=aux->next->next;
			return;
		}
		aux=aux->next;
	}
}


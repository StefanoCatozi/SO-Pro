#include <stdio.h>
#include <stdlib.h>
#include "so_game_protocol.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#define BUF 200000
#define BUFUDP 100


void InviaUDP(int sock,struct sockaddr_in* sockaddr,PacketHeader* pack);

PacketHeader* RiceviPacchettoUDP(int sock,struct sockaddr_in* sockaddr);

void InviaTCP(PacketHeader* packet, int sock);

PacketHeader* RiceviPacchettoTCP(int sock);




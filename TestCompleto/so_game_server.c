/*
 * RISCRITTURA SERVER COMPLETA
 * */
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"
#include "linked_list_mine.h"
#include "common.h"

#define MAX_NUM_CLIENTS 10
#define MAX_CONN_QUEUE  3
#define N 1000000
#define PORT 22500
#define PORTUDP 22501 
#define BUF 1000000
#define BUFUDP 1500 //MASSIMO VALORE

World world;
linked_list* clients_list; //contiene client connessi
Image* surface_texture;
Image* surface_elevation;
Image* vehicle_texture;

void sendTCPPacket(const PacketHeader* packet, int sock){
	char buf[BUF],size[4];
    int tot,sended=0,ret;
    tot = Packet_serialize(buf,packet);
    while(sended<tot){
        ret = send(sock,buf+sended,BUF,0);
        if(ret<0){
            perror("CLIENT:Errore nell'invio del pacchetto");
            return;
        }
        sended += ret;
    }
    int* s = (int*)size;
    *s = tot;
    ret = send(sock,size,4,0);
    if(ret<0){
            perror("CLIENT:Errore nell'invio del pacchetto");
            return;
        }
    return;
}

PacketHeader* receiveAndDeserializeTCP(int sock){
	char buf[BUF],size[4];
	int ret;
  ret = recv(sock,buf,BUF,MSG_WAITALL);
  if(ret<0){
    perror("CLIENT:Errore ricezione TCP");
    return NULL;
  }
  ret = recv(sock,size,4,MSG_WAITALL);
  if(ret<0){
    perror("CLIENT:Errore ricezione TCP");
    return NULL;
  }
  int* s = (int*)size;
  return Packet_deserialize(buf,*s);
}  


PacketHeader* receiveAndDeserializeUDP(int sock,struct sockaddr_in* sockaddr){
  char buf[BUFUDP],size[4];
  int ret;
  int dim = sizeof(struct sockaddr_in);
  ret = recvfrom(sock,buf,BUFUDP,0,(struct sockaddr*)sockaddr,(socklen_t*)&dim);
  if(ret<0){
    fprintf(stderr,"CLIENT:Errore ricezione UDP\n");
    return NULL;
  }
  ret = recvfrom(sock,size,4,MSG_WAITALL,(struct sockaddr*)sockaddr,(socklen_t*)&dim);
  if(ret<0){
    fprintf(stderr,"CLIENT:Errore ricezione TCP\n");
    perror("dio");
    return NULL;
  }
  int* s = (int*)size;
  printf("\t\tDeserializzo\n");
  return Packet_deserialize(buf,*s);
}

void sendUDPPacket(int sock,struct sockaddr_in* sockaddr,PacketHeader* pack){
  char buf[BUFUDP],size[4];
  int sended=0,tot,ret;
  tot = Packet_serialize(buf,pack);
  
  while(sended<tot){
    ret = sendto(sock,buf,BUFUDP,0,(struct sockaddr*)sockaddr,sizeof(struct sockaddr_in));
    if(ret<0){
      perror("CLIENT:Errore invio UDP");
      return;
    }
    sended+=ret;
  }
  int* s = (int*)size;
    *s = tot;
    ret = sendto(sock,size,4,0,(struct sockaddr*)sockaddr,sizeof(struct sockaddr_in));
    if(ret<0){
            perror("CLIENT:Errore nell'invio del pacchetto");
            return;
        }
    return;
}

WorldUpdatePacket* newWUP(World* w){
	PacketHeader h;
	h.type = WorldUpdate;

	ClientUpdate* c =(ClientUpdate*) malloc(sizeof(ClientUpdate)*(w->vehicles.size));

	ListItem* item=w->vehicles.first;
	int i=0;

  	while(item){
    	Vehicle* v=(Vehicle*)item;
    	c[i].id=v->id;
    	c[i].x =v->x;
    	c[i].y =v->y;
    	c[i].theta =v->theta;
    	item=item->next;
    	i++;
 	 }

 	 WorldUpdatePacket* p = (WorldUpdatePacket*)malloc(sizeof(WorldUpdatePacket));
 	 p->header = h;
 	 p->num_vehicles = w->vehicles.size;
 	 p->updates = c;

 	 return p;
}

void* check_connection(void* args){
	int num_clients;
	Vehicle* v;
	linked_list_node* aux, *node;
	while(1){ //ciclo all infinito sui client connessi per vedere se qualcuno si disconnette //potrebbe esserci ritardo di comunicazione
		//printf("controllo connessioni, ci sono %d veicoli\n",world.vehicles.size);
		num_clients=world.vehicles.size;
		aux=clients_list->head;
		while(aux!=NULL){
			if(recv(aux->tcp_socket, NULL, 0, MSG_PEEK | MSG_DONTWAIT) == 0){
				printf("Il client:id=%d si è disconnesso\n", aux->id);
				v=World_getVehicle(&world, aux->id);
				World_detachVehicle(&world, v);
				linked_list_delete(clients_list, aux->id);  //importante eliminare il client dalla lista altrimenti mi dira sempre disconnesso
			}
			aux=aux->next;
		}
		//Importante altrimenti non da tempo alla funzione collegamento di procedere in quanto al secondo ciclo troverà 0 nella recv
		//puo portare a molto ritardo
		//soluzione?
		//semafori?
		sleep(10);
	}
}

void* server_udp(void* args){
	int ret;
	int sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	ERROR_HELPER(sock, "Error creating udp socket");

	struct sockaddr_in addr, other_addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_port = htons(22501);

	ret=bind(sock, (struct sockaddr*) &addr, sizeof(addr));
	ERROR_HELPER(ret, "Error binding udp socket");

	Vehicle* vehicle_aux;
	//char buf[1024];
	int len=sizeof(other_addr);

	PacketHeader* packet;
	VehicleUpdatePacket* v_update;
	ImagePacket* texture;
	IdPacket* id_packet;

	while(1){
		struct sockaddr_in other_addr;
		packet=receiveAndDeserializeUDP(sock, &other_addr);
		switch(packet->type){
			case GetTexture:
								id_packet=(IdPacket*) packet; //il client mi sta chiedendo la texture di un giocatore, prima mi comunica il suo id
								//mi devo mettere in ascolto in tcp sulla socket di id_packet->id
								vehicle_aux=World_getVehicle(&world, id_packet->id);
								PacketHeader head;
								ImagePacket* send=(ImagePacket*)malloc(sizeof(ImagePacket));
								head.type=PostTexture;
								head.size=sizeof(ImagePacket);
								send->header=head;
								send->id=id_packet->id;
								send->image=vehicle_aux->texture;
								sendUDPPacket(sock,&other_addr,(PacketHeader*)send);
								/*linked_list_node* tmp=linked_list_get(clients_list, id_packet->id);
								texture=(ImagePacket*) receiveAndDeserializeTCP(tmp->u_socket); //il client mi invia l'id di chi vuole la texture
								texture->header.type=PostTexture;
								texture->image=World_getVehicle(&world, texture->id)->texture;
								sendTCPPacket((const PacketHeader*) texture, tmp->udp_socket);
								* */
								
								break;

			case VehicleUpdate:
								v_update =(VehicleUpdatePacket*) packet;
								                                
								vehicle_aux=World_getVehicle(&world, v_update->id);
								vehicle_aux->translational_force_update=v_update->translational_force;
								vehicle_aux->rotational_force_update=v_update->rotational_force;

								World_update(&world);

								sendUDPPacket( sock, &other_addr,(PacketHeader*)newWUP(&world));
								break;

			default: break;
		}
	}
	//pthread_exit(NULL);
}
Image* collegamento(int sock, IdPacket* id_packet, Image* default_texture){
    int ret, ok;
    linked_list_node* node, aux;

    printf("Sono nella fase collegamento con il client\n");
    node=linked_list_add(clients_list, id_packet->id, sock);
    sendTCPPacket((const PacketHeader*) id_packet, sock);
		
		
    Image* clientTexture;
    PacketHeader* pack;
    ImagePacket* clientImage;
	pack = receiveAndDeserializeTCP(sock); 
    clientImage = (ImagePacket*) pack;
	
	//DA ERRORE
	clientTexture=clientImage->image;
	
	ImagePacket* mapElevation=(ImagePacket*) receiveAndDeserializeTCP(sock);
	//printf("Sono qui.....\n");
	mapElevation->header.type=PostElevation;
    mapElevation->id=0;
    mapElevation->image=surface_elevation;
    
    sendTCPPacket((PacketHeader*) mapElevation, sock);
		
    ImagePacket* mapTexture=(ImagePacket*) receiveAndDeserializeTCP(sock);
    mapTexture->header.type=PostTexture;
    mapTexture->id=0;
    mapTexture->image=surface_texture;
    sendTCPPacket((const PacketHeader*) mapTexture, sock);
		
    ImagePacket* rinv=(ImagePacket*)malloc(sizeof(ImagePacket));
	PacketHeader h;
	h.type=PostTexture;
	h.size=sizeof(ImagePacket);
	rinv->header=h;
	rinv->id=id_packet->id;
	rinv->image=vehicle_texture;
	sendTCPPacket((PacketHeader*)rinv,sock);
		
    if (clientImage->header.type==GetTexture){
        //printf("[SERVER] Non ho ricevuto una texture, invio quella di default\n");
        clientImage->header.type=PostTexture;
        clientImage->id=0;
        clientImage->image=default_texture;
        clientTexture=default_texture;
        sendTCPPacket((const PacketHeader*) clientImage, sock);
    }
	
    //linked_list_set_texture(node, clientTexture);
    node->texture=clientTexture;
    return clientTexture;
}

void* accept_client(){
		int id=1,ret;
  	int sock=socket(AF_INET, SOCK_STREAM, 0);
  	ERROR_HELPER(sock, "Error creating socket");

  	struct sockaddr_in server_addr;
  	server_addr.sin_family = AF_INET;
  	server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  	server_addr.sin_port = htons(PORT);

  	int len=sizeof(struct sockaddr_in);
  	ret=bind(sock, (struct sockaddr*) &server_addr, len);
  	ERROR_HELPER(ret, "Error binding socket");

  	ret=listen(sock, MAX_CONN_QUEUE);
  	ERROR_HELPER(ret, "Error listen socket");

  	int state, client_sock;
  	struct sockaddr_in *client_addr = calloc(1, sizeof(struct sockaddr_in));
  	
		Vehicle* vehicle;
    linked_list_node* node;

	while(1){
		client_sock=accept(sock, (struct sockaddr*)client_addr, (socklen_t*) &len);
	  	ERROR_HELPER(client_sock, "Error accept scoket");

	  	//ricevo richiesta id
	  	IdPacket* id_packet=(IdPacket*) receiveAndDeserializeTCP(client_sock);
        if (id_packet==NULL) continue; //client si è sconnesso

        state=id_packet->id;
        id_packet->id=id;
        Image* clientTexture;
        
				
				
				//altra soluzione tramite thread .... porto il tutto in una funzione chiamata dal thread passando id_packet...
        if (state==-1){     //SUPER ATTENZIONE
			  clientTexture=collegamento(client_sock, id_packet, vehicle_texture); 
        }
        if (clientTexture==NULL) continue; //se è null significa che il client non è riuscito a loggare

		//printf("Terminata connessione TCP con client\n");
		vehicle=(Vehicle*) malloc(sizeof(Vehicle));
		Vehicle_init(vehicle, &world, id_packet->id, clientTexture);
		
		printf("è presente un nuovo giocatore nella mappa con id:%d\n",id_packet->id);
		World_addVehicle(&world, vehicle);
		
		//per il nuovo client_sock
		client_addr = calloc(1, sizeof(struct sockaddr_in));

		//if (state!=-1) 
		id++;
	}
	World_destroy(&world);
	
}


int main(int argc, char **argv) {
	int ret;
  if (argc<3) {
    printf("usage: %s <elevation_image> <texture_image>\n", argv[1]);
    exit(-1);
  }
  char* elevation_filename=argv[1];
  char* texture_filename=argv[2];
  char* vehicle_texture_filename="./images/arrow-right.ppm";
  printf("loading elevation image from %s ... ", elevation_filename);

  // load the images
  surface_elevation = Image_load(elevation_filename);
  if (surface_elevation) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }


  printf("loading texture image from %s ... ", texture_filename);
  surface_texture = Image_load(texture_filename);
  if (surface_texture) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }

  printf("loading vehicle texture (default) from %s ... ", vehicle_texture_filename);
  vehicle_texture = Image_load(vehicle_texture_filename);
  if (vehicle_texture) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }
  printf("Partito!\n");
	
	//inizializzo il mondo su cui connettere i clients
	World_init(&world, surface_elevation, surface_texture,  0.5, 0.5, 0.5);
	
	//devo mantenere una lista di tutti i client che sono attualmente connessi
	clients_list=linked_list_new();
	
	//UDP 
	//il server riceve periodicamente aggiornamenti dal client
	//forma del messaggio ricevuto <timestamp, translational acceleration, rotational acceleration>
	pthread_t udp_thread, check_thread, tcp_thread;

	ret=pthread_create(&udp_thread, NULL, server_udp, NULL);
  PTHREAD_ERROR_HELPER(ret, "Error creating thread");

	ret=pthread_create(&check_thread, NULL, check_connection, NULL);
  PTHREAD_ERROR_HELPER(ret, "Error creating thread");
	
	
	//TCP
	//registro un nuovo client quando si connette
	//rimuovo un client quando si disconnette
	//invio la mappa quando la richiede
	ret=pthread_create(&tcp_thread, NULL, accept_client, NULL);
  PTHREAD_ERROR_HELPER(ret, "Error creating thread");
		
	//mi basta attendere solo una "funzione thread" che looppa all infinito
	pthread_join(tcp_thread,NULL);
		
	//non posso richiamare la funzione, perche i thread e l esecuzione del main sono indipendenti
  //World_destroy(&world);
  return 0;             
}

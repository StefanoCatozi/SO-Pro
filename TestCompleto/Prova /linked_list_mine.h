#include <stdio.h>
#include <stdlib.h>
#include "image.h"

typedef struct linked_list_node{
	int tcp_socket;
	int id;
    Image* texture;
	struct linked_list_node* next;
}linked_list_node;

typedef struct{
	int size;
	linked_list_node* head;					//puntatore alla testa della struttura  //cosi piu facile riprendera nel controllo dei client disconnessi
} linked_list;


linked_list* linked_list_new();

linked_list_node* linked_list_get(linked_list* l, int id);

linked_list_node* linked_list_add(linked_list* l, int id, int tcp_socket);

void linked_list_delete(linked_list* l, int id);

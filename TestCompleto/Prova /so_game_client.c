#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"

#define PORT 22500
#define PORTUDP 22501
#define BUF 1000000
#define BUFUDP 1024
int window;
int sockTCP;
int sockUDP;
int my_id;
int num_vehicles_att;

WorldViewer viewer;
World world;
Vehicle* vehicle; // The vehicle

typedef struct{
  struct sockaddr_in* sockaddr;
}thread_args;

void InviaUDP(int sock,struct sockaddr_in* sockaddr,PacketHeader* pack){
  char buf[BUFUDP];
  int sended=0,tot,ret;
  tot = Packet_serialize(buf,pack);
  
  while(sended<tot){
    ret = sendto(sock,buf,BUFUDP,0,(struct sockaddr*)sockaddr,sizeof(struct sockaddr_in));
    if(ret<0){
      fprintf(stderr,"CLIENT:Errore invio UDP\n");
      return;
    }
    sended+=ret;
  }
}

PacketHeader* RiceviPacchettoUDP(int sock,struct sockaddr_in* sockaddr){
  char buf[BUFUDP];
  int ret;
  int dim = sizeof(struct sockaddr_in);
  ret = recvfrom(sock,buf,BUFUDP,0,(struct sockaddr*)sockaddr,(socklen_t*)&dim);
  if(ret<0){
    fprintf(stderr,"CLIENT:Errore ricezione UDP\n");
    return NULL;
  }
  return (PacketHeader*)Packet_deserialize(buf,ret);
}
void AggiungiPlayer(int id,struct sockaddr_in* sockaddr){
  PacketHeader* rcv;
  PacketHeader head;
  ImagePacket* send = (ImagePacket*)malloc(sizeof(ImagePacket));
  ImagePacket* img;
  Image* texture;
  
  head.type = GetTexture;
  head.size = sizeof(ImagePacket);
  send->header = head;
  send->id = my_id;
  send->image = NULL;
  
  InviaUDP(sockUDP,sockaddr,(PacketHeader*)send);
  rcv = RiceviPacchettoUDP(sockUDP,sockaddr);
  img = (ImagePacket*)rcv;
  texture = img->image;
  printf("Ricevuta Texture per player id:%d\n",id);
  Vehicle* new = (Vehicle*)malloc(sizeof(Vehicle));
  Vehicle_init(new,&world,id,texture);
  new = World_addVehicle(&world,new);
  return;
}
void AggiornaMondo(WorldUpdatePacket* update,struct sockaddr_in* sockaddr){
  int num = update->num_vehicles;
  int i=0;
  /*if(num<num_vehicles_att){
    ListHead veicoli_mondo = world.vehicles;
    ClientUpdate* veicoli_update = update.updates;
    ClientUpdate up;
    ListItem* item = veicoli_mondo.first;
    while(i<num_vehicles_att){
      up = veicoli_update[i];
      Vehicle* v = (Vehicle*)item;
      if(up.id == v->id) EliminaPlayer(v->id,
      item = item->next;
      i++;
    }
    num_vehicles_att = num;
  }*/
  
  if(num>num_vehicles_att){
    ClientUpdate* veicoli_update = update->updates;
    ClientUpdate up;
    //int diff = num_vehicles_att-num;
    i=num_vehicles_att;
    while(i<num){
      up = veicoli_update[i];
      printf("Aggiungo Nuovo veicolo con id:%d\n",up.id);
      AggiungiPlayer(up.id,sockaddr);
      i++;
    }
    num_vehicles_att = num;
  }
  i=0;
  ClientUpdate client_update;
  Vehicle* vec;
  while(i<num){
    printf("Aggiorno veicolo numero:%d\n",i);
    client_update = update->updates[i];
    vec = World_getVehicle(&world,client_update.id);
    vec->x = client_update.x;
    vec->y = client_update.y;
    vec->theta = client_update.theta;
  }
  World_update(&world);
}

void* HandlerConnessione(void* args){
  int flag = 1;
  thread_args* arg = (thread_args*)args;
  PacketHeader* update;
  
  VehicleUpdatePacket* vupd_pack = (VehicleUpdatePacket*)malloc(sizeof(VehicleUpdatePacket));
  PacketHeader header;
  header.type = VehicleUpdate;
  vupd_pack->header = header;
  vupd_pack->id = my_id;
  while(flag){        //METTERE UN SIGNALHANDLER
    vupd_pack->rotational_force = vehicle->rotational_force_update;
    vupd_pack->translational_force = vehicle->translational_force_update;
    InviaUDP(sockUDP,arg->sockaddr,(PacketHeader*)vupd_pack);
    
    update = RiceviPacchettoUDP(sockUDP,arg->sockaddr);
    //NEW PALYER????
    switch (update->type){
    case WorldUpdate: AggiornaMondo((WorldUpdatePacket*)update,arg->sockaddr);
                        break;
    /*case VehicleUpdate: AggiungiPlayer((VehicleUpdatePacket*)update,arg->sockaddr); //TEST AGGIUNGERE PLAYER
                        break;*/                                                      //CON SEGNALAZIONE DA SERVER
    default: break;
    }
  }
}

void InviaTCP(PacketHeader* packet, int sock){
	char buf[BUF];
    int tot,sended=0,ret;
    printf("\t\tSERIALIZZO\n");
    tot = Packet_serialize(buf,packet);
    printf("\t\tTOT:%d\n",tot);
    while(sended<tot){
        printf("\t\tINVIATCP\n\t\tsended:%d\n",sended);
        ret = send(sock,buf+sended,BUF,0);
        printf("\t\t%d\n",ret);
        if(ret<0){
            fprintf(stderr,"CLIENT:Errore nell'invio del pacchetto\n");
            return;
        }
        sended += ret;
    }
    return;
}

PacketHeader* RiceviPacchettoTCP(int sock){
	char buf[BUF];
	int ret;
  ret = recv(sock,buf,BUF,MSG_WAITALL);
  if(ret<0){
    fprintf(stderr,"CLIENT:Errore ricezione TCP\n");
    return NULL;
  }
  return Packet_deserialize(buf,ret);
}  
/*  
void keyPressed(unsigned char key, int x, int y)
{
  switch(key){
  case 27:
    glutDestroyWindow(window);
    exit(0);
  case ' ':
    vehicle->translational_force_update = 0;
    vehicle->rotational_force_update = 0;
    break;
  case '+':
    viewer.zoom *= 1.1f;
    break;
  case '-':
    viewer.zoom /= 1.1f;
    break;
  case '1':
    viewer.view_type = Inside;
    break;
  case '2':
    viewer.view_type = Outside;
    break;
  case '3':
    viewer.view_type = Global;
    break;
  }
}


void specialInput(int key, int x, int y) {
  switch(key){
  case GLUT_KEY_UP:
    vehicle->translational_force_update += 0.1;
    break;
  case GLUT_KEY_DOWN:
    vehicle->translational_force_update -= 0.1;
    break;
  case GLUT_KEY_LEFT:
    vehicle->rotational_force_update += 0.1;
    break;
  case GLUT_KEY_RIGHT:
    vehicle->rotational_force_update -= 0.1;
    break;
  case GLUT_KEY_PAGE_UP:
    viewer.camera_z+=0.1;
    break;
  case GLUT_KEY_PAGE_DOWN:
    viewer.camera_z-=0.1;
    break;
  }
}


void display(void) {
  WorldViewer_draw(&viewer);
}


void reshape(int width, int height) {
  WorldViewer_reshapeViewport(&viewer, width, height);
}

void idle(void) {
  World_update(&world);
  usleep(30000);
  glutPostRedisplay();
  
  // decay the commands
  vehicle->translational_force_update *= 0.999;
  vehicle->rotational_force_update *= 0.7;
}
*/
int main(int argc, char **argv) {
  if (argc<3) {
    printf("usage: %s <server_address> <player texture>\n", argv[1]);
    exit(-1);
  }

  printf("loading texture image from %s ... ", argv[2]);
  Image* my_texture = Image_load(argv[2]);
  if (my_texture) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }
  
  Image* my_texture_for_server;
  // todo: connect to the server
  //   -get ad id
  //   -send your texture to the server (so that all can see you)!!!!!!!!!!!
  //   -get an elevation map
  //   -get the texture of the surface

  //CONNESSIONE
  char* ip = argv[1];
  struct sockaddr_in* sckaddr = malloc(sizeof(struct sockaddr_in));
  int sockTCP = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if(sockTCP < 0){
    fprintf(stderr,"CLIENT: Errore creazione socket\n");
    return 0;
  }
  
  sckaddr->sin_family = AF_INET;
  sckaddr->sin_addr.s_addr = inet_addr(ip);
  sckaddr->sin_port = htons(PORT);
  int n = sizeof(*sckaddr);
  if(connect(sockTCP,(struct sockaddr*) sckaddr, n)<0){
    fprintf(stderr,"CLIENT:Errore connessione\n");
  }
  printf("Connessione TCP avviata\n");
  //INIZIALIZZAZIONE
  // these come from the server
  int my_id;
  Image* map_elevation;
  Image* map_texture;
  Image* my_texture_from_server;
  
  //Prendo id
  
  PacketHeader head;
  PacketHeader* pack;
  
  IdPacket* id_pack = (IdPacket*)malloc(sizeof(IdPacket));
  head.type = GetId;
  head.size = sizeof(IdPacket);
  id_pack->header = head;
  id_pack->id = -1;
  InviaTCP((PacketHeader*)id_pack,sockTCP);
  printf("ID Inviato\n");
  pack = RiceviPacchettoTCP(sockTCP);
  printf("RICEVUTO PACCHETTO\n");
  id_pack = (IdPacket*)pack;
  my_id = id_pack->id;
  printf("Ottenuto id:%d\n",my_id);
  //MANDO TEXTURE !!!!
  ImagePacket* img = (ImagePacket*)malloc(sizeof(ImagePacket));
  head.type = PostTexture;
  head.size = sizeof(ImagePacket);
  printf("adsdasdasd\n");
  img->header = head;
  img->id = my_id;
  img->image = my_texture;
  printf("Asdefrgrg\n");
  InviaTCP((PacketHeader*)img,sockTCP);
  printf("Texture Inviata\n");
  //Prendo Elevation
  head.type = GetElevation;
  img->header = head;
  img->id = my_id;
  InviaTCP((PacketHeader*)img,sockTCP);
  pack = RiceviPacchettoTCP(sockTCP);
  img = (ImagePacket*)pack;
  map_elevation = img->image;
  printf("Ricevuta Elevation Map\n");
  //Prendo Surface
  head.type = GetTexture;
  img->header =  head;
  img->id = my_id;
  printf("asdad\n");
  InviaTCP((PacketHeader*)img,sockTCP);
  printf("Inviata Surface\n");
  pack = RiceviPacchettoTCP(sockTCP);
  img = (ImagePacket*)pack;
  map_texture = img->image;
  printf("Ricevuta Surface\n");
  //MYTEXTURE
  pack = RiceviPacchettoTCP(sockTCP);
  img = (ImagePacket*)pack;
  my_texture_from_server = img->image;
  printf("Ricevuta Texture dal server \n");
  //my_texture_from_server = my_texture //?????????
  // construct the world
  World_init(&world, map_elevation, map_texture, 0.5, 0.5, 0.5);
  vehicle=(Vehicle*) malloc(sizeof(Vehicle));
  Vehicle_init(vehicle, &world, my_id, my_texture_from_server);
  World_addVehicle(&world, vehicle);

  // spawn a thread that will listen the update messages from
  // the server, and sends back the controls
  // the update for yourself are written in the desired_*_force
  // fields of the vehicle variable
  // when the server notifies a new player has joined the game
  // request the texture and add the player to the pool
  /*FILLME*/
  
  sockUDP = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  
  struct sockaddr_in* udp = malloc(sizeof(struct sockaddr_in));
  udp->sin_family = AF_INET;
  udp->sin_addr.s_addr = inet_addr(ip);
  udp->sin_port = htons(PORTUDP);
  
  if(connect(sockUDP,(struct sockaddr*) udp,sizeof(struct sockaddr_in))<0){
    fprintf(stderr,"CLIENT:Errore connessione\n");
  }
  printf("Aperta Connessione UDP\n");
  thread_args args;
  args.sockaddr = udp;
  pthread_t thread;
  pthread_create(&thread,NULL,HandlerConnessione,(void*)&args);
  pthread_detach(thread);
  printf("Thread UDP partito\n");
  WorldViewer_runGlobal(&world, vehicle, &argc, argv);

  // cleanup
  World_destroy(&world);
  return 0;             
}



 
 
 

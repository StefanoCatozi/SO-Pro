#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"

#define BUF 1000000
#define PORT 27001
#define MAX_CONN_QUEUE 10

void InviaTCP(PacketHeader* packet, int sock){
	char buf[BUF],size[4];
    int tot,sended=0,ret;
    tot = Packet_serialize(buf,packet);
    while(sended<tot){
        ret = send(sock,buf+sended,BUF,0);
        if(ret<0){
            fprintf(stderr,"CLIENT:Errore nell'invio del pacchetto\n");
            return;
        }
        sended += ret;
    }
    int* s = (int*)size;
    *s = tot;
    ret = send(sock,size,4,0);
    if(ret<0){
            perror("CLIENT:Errore nell'invio del pacchetto");
            return;
        }
    return;
}

PacketHeader* RiceviPacchettoTCP(int sock){
	char buf[BUF],size[4];
	int ret;
  ret = recv(sock,buf,BUF,MSG_WAITALL);
  printf("\t\tRicevuto buffer:%d\n",ret);
  if(ret<0){
    fprintf(stderr,"CLIENT:Errore ricezione TCP\n");
    perror("dio");
    return NULL;
  }
  ret = recv(sock,size,4,MSG_WAITALL);
  if(ret<0){
    fprintf(stderr,"CLIENT:Errore ricezione TCP\n");
    perror("dio");
    return NULL;
  }
  int* s = (int*)size;
  printf("\t\tDeserializzo\n");
  return Packet_deserialize(buf,*s);
}  

int main(){
  int sockTCP;
  sockTCP = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
  if(sockTCP<0){
    printf("asdSAD\n");
    return 1;
  }
  struct sockaddr_in addrTCP;
  addrTCP.sin_family = AF_INET;
  addrTCP.sin_addr.s_addr = inet_addr("127.0.0.1");
  addrTCP.sin_port = htons(PORT);
  
  if(bind(sockTCP,(struct sockaddr*)&addrTCP,sizeof(struct sockaddr_in))<0){
    printf("asdsadasda\n");
    return 1;
  }
  
  if(listen(sockTCP,MAX_CONN_QUEUE)<0){
    printf("asdasd\n");
    return 1;
  }
  int client_sock;
  struct sockaddr_in client_addr;
  int addr_len = sizeof(struct sockaddr_in);
  client_sock = accept(sockTCP,(struct sockaddr*)&client_addr,(socklen_t*)&addr_len);
  if(client_sock<0){
    printf("asdasd\n");
    return 1;
  }
  printf("Connddesso\n");
  PacketHeader* pack;
  pack = RiceviPacchettoTCP(client_sock);
  printf("RicievutoPaccjettasdsadao\n");
  IdPacket* id = (IdPacket*)pack;
  printf("ricevuto id:%d\n",id->id);
  id->id--;
  InviaTCP((PacketHeader*)id,client_sock);

  PacketHeader head;
  IdPacket* idd =(IdPacket*)malloc(sizeof(IdPacket));
  head.type = GetId;
  head.size = sizeof(IdPacket);
  idd->header = head;
  idd->id = 50;
  InviaTCP((PacketHeader*)idd,client_sock);
  printf("Inviato Pacchetto id:%d\n",idd->id);
  pack = RiceviPacchettoTCP(client_sock);
  idd = (IdPacket*)pack;
  printf("Ricevuto Pacchetto:%d\n",idd->id);

  printf("\n\n\n");
  ImagePacket* image;
  printf("Ricevo Immagine\n");
  image = (ImagePacket*)RiceviPacchettoTCP(client_sock);
  printf("Ricevuta immagine da id:");
  printf("%d\n",image->id);
  
  char* vehicle_texture_filename="./images/arrow-right.ppm";
  Image* vehicle_texture = Image_load(vehicle_texture_filename);
  if (vehicle_texture) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }
  image->header.type = PostTexture;
  image->id = 0;
  image->image = vehicle_texture;
  InviaTCP((PacketHeader*)image,client_sock);
  printf("Immagine Inviata\n");
  
  return 0;
}

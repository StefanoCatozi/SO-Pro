#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"

#define BUF 1000000
#define PORT 27001
#define MAX_CONN_QUEUE 10

int sockTCP;
int sockUDP;

void InviaTCP(PacketHeader* packet, int sock){
	char buf[BUF],size[4];
    int tot,sended=0,ret;
    tot = Packet_serialize(buf,packet);
    while(sended<tot){
        ret = send(sock,buf+sended,BUF,0);
        if(ret<0){
            fprintf(stderr,"CLIENT:Errore nell'invio del pacchetto\n");
            return;
        }
        sended += ret;
    }
    int* s = (int*)size;
    *s = tot;
    ret = send(sock,size,4,0);
    if(ret<0){
            perror("CLIENT:Errore nell'invio del pacchetto");
            return;
        }
    return;
}

PacketHeader* RiceviPacchettoTCP(int sock){
	char buf[BUF],size[4];
	int ret;
  ret = recv(sock,buf,BUF,MSG_WAITALL);
  printf("\t\tRicevuto buffer:%d\n",ret);
  if(ret<0){
    fprintf(stderr,"CLIENT:Errore ricezione TCP\n");
    perror("dio");
    return NULL;
  }
  ret = recv(sock,size,4,MSG_WAITALL);
  if(ret<0){
    fprintf(stderr,"CLIENT:Errore ricezione TCP\n");
    perror("dio");
    return NULL;
  }
  int* s = (int*)size;
  printf("\t\tDeserializzo\n");
  return Packet_deserialize(buf,*s);
}  
   

int main(){
  printf("asdsad\n");
  
  
    struct sockaddr_in* sckaddr = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
  int sockTCP = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if(sockTCP < 0){
    fprintf(stderr,"CLIENT: Errore creazione socket\n");
    return 0;
  }
  
  sckaddr->sin_family = AF_INET;
  sckaddr->sin_addr.s_addr = inet_addr("127.0.0.1");
  sckaddr->sin_port = htons(PORT);
  if(connect(sockTCP,(struct sockaddr*) sckaddr, sizeof(struct sockaddr_in))<0){
    fprintf(stderr,"CLIENT:Errore connessione\n");
  }
  PacketHeader* pack;
  PacketHeader head;
  head.type = GetId;
  head.size = sizeof(IdPacket);
  IdPacket* id = (IdPacket*)malloc(sizeof(IdPacket));
  id->id = 23;
  id->header = head;
  printf("iddd:%d\n",id->id);
  // Packet_free(&id->header);
  char buf[BUF];
  int size = Packet_serialize(buf,(PacketHeader*)id);
  pack = Packet_deserialize(buf,size);
  IdPacket* asd = (IdPacket*)pack;
  //asd->id++;
  printf("MESS: %d\n\n",asd->id);
  
  InviaTCP((PacketHeader*)id,sockTCP);
  printf("Inviato:%d\n",id->id);
  pack = RiceviPacchettoTCP(sockTCP);
  IdPacket* ad = (IdPacket*)pack;
  printf("Pacchetto Ricevuto:%d\n",ad->id);
  
  pack = RiceviPacchettoTCP(sockTCP);
  IdPacket* add = (IdPacket*)pack;
  printf("Ricevuto Pacchetto:%d\n",add->id);
  add->id = add->id+3;
  InviaTCP((PacketHeader*)add,sockTCP);
  
  printf("\n\n\n");
  ImagePacket* image = (ImagePacket*)malloc(sizeof(ImagePacket));
  head.type = GetTexture;
  head.size = sizeof(ImagePacket);
  image->id = 1;
  image->header = head;
  image->image = NULL;
  printf("Invio GetTexture\n");
  InviaTCP((PacketHeader*)image,sockTCP);
  printf("Inviato GetTexture\n");
  
  ImagePacket* im;
  printf("Ricevo Post");
  im = (ImagePacket*) RiceviPacchettoTCP(sockTCP);
  if(im == 0) printf("ErroreMAdonnaLadra\n");
  printf("Ricevuta immagine con id:");
  printf("%d \n",im->id);
  
  return 0;
}
